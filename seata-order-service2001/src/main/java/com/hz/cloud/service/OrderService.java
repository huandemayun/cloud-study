package com.hz.cloud.service;

import com.hz.cloud.entities.Order;

public interface OrderService {
    /**
     * 创建订单
     */
    void create(Order order);
}
