package com.hz.cloud.mapper;

import com.hz.cloud.entities.Order;
import tk.mybatis.mapper.common.Mapper;

public interface OrderMapper extends Mapper<Order> {
}