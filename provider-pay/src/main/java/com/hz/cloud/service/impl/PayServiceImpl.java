package com.hz.cloud.service.impl;

import com.hz.cloud.entities.Pay;
import com.hz.cloud.mapper.PayMapper;
import com.hz.cloud.service.PayService;
import com.hz.cloud.service.TestService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Date 2024/3/11 21:41
 * @author zhanghuan
 */
@Service
public class PayServiceImpl implements PayService {
    @Resource
    private PayMapper payMapper;

    @Override
    public int add(Pay pay) {
        return payMapper.insertSelective(pay);
    }

    @Override
    public int update(Pay pay) {
        return payMapper.updateByPrimaryKeySelective(pay);
    }

    @Override
    public int delete(Integer id) {
        return payMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Pay getPayById(Integer id) {
        return payMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Pay> getAll() {
        return payMapper.selectAll();
    }


    @Resource
    TestService testService;

    @Override
    public String test() {
        return testService.test01();
    }


}
