package com.hz.cloud.service;
import com.hz.cloud.entities.Pay;
import java.util.List;

/**
 * Date 2024/3/11 21:41
 * @author zhanghuan
 */
public interface PayService {
    public int add(Pay pay);
    public int update(Pay pay);
    public int delete(Integer id);

    public Pay getPayById(Integer id);

    public List<Pay> getAll();

    String test();

}
