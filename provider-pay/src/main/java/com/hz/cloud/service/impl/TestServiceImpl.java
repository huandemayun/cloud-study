package com.hz.cloud.service.impl;

import com.hz.cloud.mapper.PayMapper;
import com.hz.cloud.service.TestService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestServiceImpl implements TestService {
    @Resource
    private PayMapper payMapper;

    @Transactional(rollbackFor = Exception.class)
    public String test01() {
        payMapper.deleteByPrimaryKey(1);
        int i = 1 / 0;
        return "成功";
    }
}
