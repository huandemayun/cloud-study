package com.hz.cloud.controller;

import com.hz.cloud.entities.Pay;
import com.hz.cloud.entities.PayDTO;
import com.hz.cloud.resp.ResultData;
import com.hz.cloud.resp.ReturnCodeEnum;
import com.hz.cloud.service.PayService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Date 2024/3/11 21:41
 * @Author zhanghuan
 */
@Tag(name = "支付微服务模块",description = "支付crud")
@RestController
public class PayController {
    @Resource
    private PayService payService;

    @PostMapping("/pay/add")
    @Operation(summary = "新增支付信息",description = "新增支付信息方法")
    public ResultData<String> add(@RequestBody Pay pay) {

        return payService.add(pay) > 0 ? ResultData.success("success") : ResultData.fail(ReturnCodeEnum.RC999.getCode(),"新增失败");
    }

    @PostMapping("/pay/update")
    public ResultData<String> update(@RequestBody PayDTO payDTO) {
        Pay pay = new Pay();
        BeanUtils.copyProperties(payDTO, pay);
        return payService.update(pay) > 0 ? ResultData.success("success") : ResultData.fail(ReturnCodeEnum.RC999.getCode(),"修改失败");
    }

    @PostMapping("/pay/delete/{id}")
    public ResultData<String> delete(@PathVariable("id") Integer id) {
        return payService.delete(id) > 0  ? ResultData.success("success") : ResultData.fail(ReturnCodeEnum.RC999.getCode(),"删除失败");
    }

    @GetMapping("/pay/getPayById/{id}")
    public ResultData<PayDTO>  getPayById(@PathVariable("id") Integer id) {
        PayDTO payDTO = new PayDTO();
        BeanUtils.copyProperties(payService.getPayById(id),payDTO);
        return ResultData.success(payDTO);
    }

    @GetMapping("/pay/getAll")
    public ResultData<List<Pay>> getAll() {
        return ResultData.success( payService.getAll());
    }

    @Value("${server.port}")
    private String port;

    @GetMapping("/pay/get/Info")
    public String getInfoConsul(@Value("${hz.info}")String hzInfo) {
        return "provider-pay:" + hzInfo + "\t" + "port:" + port;
    }

    @GetMapping("/pay/test")
    public String test(){
        return payService.test();
    }

}
