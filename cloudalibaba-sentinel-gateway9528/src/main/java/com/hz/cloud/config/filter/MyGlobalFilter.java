package com.hz.cloud.config.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


/**
 * 自定义全局过滤器
 */
@Component
@Slf4j
public class MyGlobalFilter implements GlobalFilter, Ordered {
    private static final String BEGIN_VISIT_TIME = "begin_visit_time";

    /**
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //1.先记录下访问接口的开始时间
        exchange.getAttributes().put(BEGIN_VISIT_TIME, System.currentTimeMillis());

        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
            //2.获取结束时间
            Long beginTime = exchange.getAttribute(BEGIN_VISIT_TIME);
            if (beginTime != null) {
                log.info("访问接口主机" + exchange.getRequest().getURI().getHost());
                log.info("访问接口端口" + exchange.getRequest().getURI().getPort());
                log.info("访问接口URL" + exchange.getRequest().getURI().getPath());
                log.info("访问接口URL后面参数" + exchange.getRequest().getURI().getRawQuery());
                log.info("访问接口时长" + (System.currentTimeMillis() - beginTime) + "ms");
                log.info("======================分割线===========================");
                System.out.println();
            }
        }));
    }

    /**
     * 过滤器的优先级，返回值越小，优先级越高
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
