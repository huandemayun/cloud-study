package com.hz.cloud.controller;
import com.hz.cloud.entities.PayDTO;
import com.hz.cloud.resp.ResultData;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Date 2024/3/14 7:41
 * @author zhanghuan
 */
@RestController
public class OrderController {
    private static final String PaymentSrv_URL = "http://cloud-payment-service";

    @Resource
    private RestTemplate restTemplate;

    @PostMapping("/consumer/pay/add")
    public ResultData addOrder(PayDTO payDTO) {
        return restTemplate.postForObject(PaymentSrv_URL + "/pay/add",payDTO, ResultData.class);
    }

    @GetMapping("/consumer/pay/get/{id}")
    public ResultData getPayInfo(@PathVariable("id") Integer id) {
        return restTemplate.getForObject(PaymentSrv_URL + "/pay/getPayById/"+id, ResultData.class,id);
    }

    @GetMapping("/consumer/pay/get/Info")
    public String getInfoConsul() {
        return restTemplate.getForObject(PaymentSrv_URL + "/pay/get/Info", String.class);
    }

}
