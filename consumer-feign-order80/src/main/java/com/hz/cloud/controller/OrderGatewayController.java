package com.hz.cloud.controller;

import com.hz.cloud.apis.PayFeignApi;
import com.hz.cloud.resp.ResultData;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderGatewayController {
    @Resource
    PayFeignApi payFeignApi;

    /**
     * GateWay进行网关测试案例01
     * @param id
     * @return
     */
    @GetMapping(value = "/feign/pay/gateway/get/{id}")
    public ResultData getById(@PathVariable("id") Integer id){
        return payFeignApi.getById(id);
    }

    /**
     * GateWay进行网关测试案例02
     * @return
     */
    @GetMapping(value = "/feign/pay/gateway/info")
    public ResultData<String> getGatewayInfo(){
        return payFeignApi.getGatewayInfo();
    }

}
