package com.hz.cloud.controller;
import com.hz.cloud.apis.PayFeignApi;
import com.hz.cloud.entities.PayDTO;
import com.hz.cloud.resp.ResultData;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * Date 2024/3/14 7:41
 * @author zhanghuan
 */
@RestController
public class OrderController {
    @Resource
    private PayFeignApi payFeignApi;

    @PostMapping("/feign/pay/add")
    public ResultData addOrder(@RequestBody PayDTO payDTO) {
        return payFeignApi.addOrder(payDTO);
    }

    @GetMapping("/feign/pay/get/{id}")
    public ResultData getPayInfo(@PathVariable("id") Integer id) {
        return payFeignApi.getPayInfo(id);
    }

    @GetMapping("/feign/pay/get/Info")
    public String mylb() {
        return payFeignApi.mylb();
    }
}
